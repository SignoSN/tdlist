import { Component, OnInit } from '@angular/core';
import { Storage } from '@ionic/storage';
import { AlertController, ToastController } from '@ionic/angular';
import { Tarea } from '../modelos/tarea';
import { Router } from '@angular/router';

@Component({
  selector: 'app-nuevo',
  templateUrl: './nuevo.page.html',
  styleUrls: ['./nuevo.page.scss'],
})
export class NuevoPage implements OnInit {
tarea : Tarea = {nombre: null};
tareas : Array<Tarea>= [];
  constructor(
  	private storage : Storage,
  	private alert : AlertController,
  	private toast : ToastController,
  	private router: Router,
  ) { }

  ngOnInit() {
  	this.storage.get('tareas')
  	.then((tareas : Array<Tarea>) => {
  		if(tareas && tareas.length > 0 ){
  			this.tareas = tareas;
  		}else{
  			this.tareas = [];
  		}
  	}).catch(error => {
  		console.log(error);
  		this.tareas = [];
  	})
  }

  async storeTarea()
  {
  	this.tareas.unshift(this.tarea);
  	await this.storage.set('tareas', this.tareas);
  	this.router.navigateByUrl('/home');
  }

}
