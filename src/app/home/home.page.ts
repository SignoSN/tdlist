import { Component } from '@angular/core';
import { Storage } from '@ionic/storage';
import { Tarea } from '../modelos/tarea';
import { Router } from '@angular/router';
import { ToastController } from '@ionic/angular';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

	tareas : Array<Tarea>;

  constructor(
  	private storage: Storage,
    private router: Router,
    private toast: ToastController,
  	) {}

  ionViewDidEnter(){
    this.getTareas();
  }

  deleteTarea(tarea){
  	let index = this.tareas.indexOf(tarea);
  	if( index != -1 ){
  		this.tareas.splice(index, 1);
  		this.storage.set('tareas', this.tareas);
  	}
  }

  async getTareas(){
    this.tareas = await this.storage.get('tareas');
  }

  async showError( mensaje ){
    const toast = await this.toast.create({
      message: mensaje,
      buttons:  [{
        text: "Cerrar",
        role: "cancel"
      }]
    });
    await toast.present();
  }

  async refreshTareas(event){
  	this.tareas = await this.storage.get("tareas");
  	event.target.complete();
  }

}
